"""Physics numerical constants."""

import numpy_ipps.constants.physics.float32
import numpy_ipps.constants.physics.float64
