"""Maths numerical constants."""

import numpy_ipps.constants.maths.float32
import numpy_ipps.constants.maths.float64
