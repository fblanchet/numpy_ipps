ChangeLog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_ and this project adheres to
`Semantic Versioning`_.

.. _Keep a Changelog: https://keepachangelog.com/
.. _Semantic Versioning: https://semver.org/

[2.2.0]
-------

Added
^^^^^

Operations:
    - Exponential and logarithm base 2 and 3
    - Rational
    - Transform, add Hilbert
    - Statistical
    - Filtering, add IIR and Median
    - Constants
    - Single


[2.1.0]
-------

Added
^^^^^

Support : Linux/Windows & Intel/AMD.


Change
^^^^^^

Split FFT in filtering and signal.


[2.0.0]
-------

Added
^^^^^

Stable interface.

Operations:
    - Broadcast

Change
^^^^^^

Split FFT in filtering and signal.


[1.0.0]
-------

Added
^^^^^

Initial release.

Operations:
    - Complex, integer and floating
    - Conversion and initialization
    - Exponential and logarithm
    - FFT
    - Rounding
    - Special
    - Trigonometric
