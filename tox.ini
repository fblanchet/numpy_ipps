[tox]
tox_pyenv_fallback=False
envlist =
    isort
    black
    flake8
    pydocstyle
    doc8
    sphinx
    py3{5,6,7}_min
    py3{8,9}_latest
    deploy

[testenv:flake8]
basepython = python3.9
depends = black
deps = 
    flake8 
    flake8-bugbear
skip_install = True
commands = flake8 -v

[testenv:isort]
basepython = python3.9
deps = 
    isort
skip_install = True
commands = isort . --recursive --apply

[testenv:black]
basepython = python3.9
depends = isort
deps = black
skip_install = True
commands = black --line-length 79 setup.py plot_benchmark.py plot_benchmark_fir.py docs/source/conf.py numpy_ipps tests

[testenv:pydocstyle]
basepython = python3.9
depends = black
deps = pydocstyle
skip_install = True
commands = pydocstyle numpy_ipps

[testenv:doc8]
basepython = python3.9
depends = black
deps =
    doc8
    pygments
skip_install = True
commands = doc8

[testenv:sphinx]
basepython = python3.8
depends =
    black
install_command =
    python -m pip install --upgrade --upgrade-strategy eager --cache-dir={toxinidir}/.cache/pip {packages} {opts}
deps = 
    breathe<4.15
    sphinx<3
    sphinxcontrib-apidoc
    sphinx_rtd_theme 
    m2r
    rst2pdf
whitelist_externals = rm
commands =
    rm -fr docs/source/api
    sphinx-build -W -b linkcheck docs/source docs/build/linkcheck
    sphinx-build -W -b html docs/source docs/build/html
    sphinx-build -W -b latex docs/source docs/build/latex
    sphinx-build -W -b man docs/source docs/build/man
    sphinx-build -W -b pdf docs/source docs/build/pdf
parallel_show_output = true

[testenv:deploy]
basepython = python3.9
deps = 
    twine
skip_install = True
whitelist_externals = 
    rm
setenv =
    TWINE_USERNAME = {env:TWINE_USERNAME}
    TWINE_PASSWORD = {env:TWINE_PASSWORD}
commands =
    rm -fr dist
    python setup.py sdist
    twine upload dist/*

[testenv]
basepython = 
    py35_{min,latest}: python3.5
    py36_{min,latest}: python3.6
    py37_{min,latest}: python3.7
    py38_{min,latest}: python3.8
    py39_{min,latest}: python3.9
depends =
    py3{5,6,7,8,9}_{min,latest}: black
install_command =
    python -m pip install --upgrade --upgrade-strategy only-if-needed --cache-dir={toxinidir}/.cache/pip --no-binary numpy --no-binary scipy {packages} {opts}
skip_install = 
    py3{5,6,7,8,9}_{min,latest}: False
deps =
    py3{5,6,7,8,9}_{min,latest}: psutil
    py3{5,6,7,8,9}_{min,latest}: pyqt5<5.15
    py3{5,6,7,8,9}_{min,latest}: pytest >= 3.1
    py3{5,6,7,8,9}_{min,latest}: pytest-benchmark
    py3{5,6,7,8,9}_{min,latest}: pytest-html
    py3{5,6,7,8,9}_{min,latest}: matplotlib

    py3{5,6,7}_min: cffi ==1.10
    py3{5,6,7}_min: py-cpuinfo ==7.0
    py3{5,6,7}_min: numpy ==1.15
    py3{5,6,7}_min: regex ==2014.1.10
    py3{5,6,7}_min: scipy ==1.1

    py3{5,6,7,8,9}_latest: cffi
    py3{5,6,7,8,9}_latest: py-cpuinfo
    py3{5,6,7,8,9}_latest: numpy
    py3{5,6,7,8,9}_latest: regex
    py3{5,6,7,8,9}_latest: scipy
setenv =
    py3{5,6,7,8,9}_{min,latest}: PYTHONOPTIMIZE = 1
    py35_min: PYTHONVERSION = 3.5
    py36_min: PYTHONVERSION = 3.6
    py37_min: PYTHONVERSION = 3.7
    py38_latest: PYTHONVERSION = 3.8
    py39_latest: PYTHONVERSION = 3.9
    py3{5,6,7}_min: PYTHONUPDATE = min
    py3{8,9}_latest: PYTHONUPDATE = latest
    py3{5,6,7,8,9}_{min,latest}: NUMPY_IPPS_DISABLE_NUMPY = 1
whitelist_externals = 
    py3{5,6,7,8,9}_{min,latest}: numactl
parallel_show_output = true
commands =
    py3{5,6,7,8,9}_{min,latest}: numactl --physcpubind=0 pytest \
    py3{8,9}_latest:                 --assert=plain \
    py3{5,6,7,8,9}_{min,latest}:     --no-header --no-summary \
    py3{5,6,7,8,9}_{min,latest}:     --benchmark-timer=time.process_time --benchmark-disable-gc \
    py3{8,9}_latest:                 --benchmark-calibration-precision=100000 \
    py3{5,6,7}_min:                  --benchmark-calibration-precision=1000 \
    py3{5,6,7,8,9}_{min,latest}:     --junitxml=log/test_{envname}.xml -o junit_suite_name=test_{envname} \
    py3{5,6,7,8,9}_{min,latest}:     --benchmark-storage=log --benchmark-save=benchmark_{envname} \
    py3{5,6,7,8,9}_{min,latest}:     --html=log/test_html_{envname}/report.html
    py3{8,9}_latest:                 python plot_benchmark.py
    py3{8,9}_latest:                 python plot_benchmark_fir.py
